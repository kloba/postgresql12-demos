-- https://www.depesz.com/2019/03/19/waiting-for-postgresql-12-partial-implementation-of-sql-json-path-language/
-- https://www.postgresql.org/docs/12/functions-json.html

DROP TABLE IF EXISTS house;

CREATE TABLE house(js) AS SELECT jsonb '
{
  "info": {
    "contacts": "Optima plaza",
    "dates": ["01-02-2015", "04-10-1957 19:28:34 +00", "12-04-1961 09:07:00 +03"]
  },
  "address": {
    "country": "Ukraine",
    "city": "Lviv",
    "street": "79000, Naukova st., 7D"
  },
  "lift": false,
  "floor": [
    {
      "level": 6,
      "rooms": [
        {"name": "Duckburg", "area": 20},
        {"name": "Elmore", "area": 16},
        {"name": "Hogsmeade", "area": null}
      ]
    },
    {
      "level": 7,
      "rooms": [
        {"name": "Vice City", "area": 50},
        {"name": "Fun room", "area": 101}
      ]
    }
  ]
}
';


SELECT jsonb_path_query(js, '$.*') FROM house;

SELECT jsonb_path_query(js, '$.floor[*].level') FROM house;

SELECT jsonb_path_query(js, '$.floor[*].rooms[*] ? (@.area > 100) ') FROM house;
