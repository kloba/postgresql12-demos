# PostgreSQL12 demos

Scripts to demonstrate and compare new features in PostgreSQL 12.
![QR](/images/qr.png)


## Patches included to PostgreSQL 12 on commitfests:
Commitfest 2019-07: https://commitfest.postgresql.org/22/?text=&status=4&targetversion=2&author=-1&reviewer=-1&sortkey=
Commitfest 2019-07: https://commitfest.postgresql.org/23/?text=&status=4&targetversion=2&author=-1&reviewer=-1&sortkey=  

## PostgreSQL 12 Features Highlights (TODO list):

- ##### Partitioning enhancements in PostgreSQL 12:
  - [x] Reduce partition tuple routing overheads (inserts into 10k partitions table) (https://commitfest.postgresql.org/20/1690/, David Rowley);
  - [x] Allow ATTACH PARTITION with only ShareUpdateExclusiveLock. (https://commitfest.postgresql.org/22/1842/, https://git.postgresql.org/gitweb/?p=postgresql.git;a=commit;h=898e5e3290a72d288923260143930fb32036c00c, Robert Haas (rhaas), Álvaro Herrera (alvherre), David Rowley (davidrowley));
  - [x] Add pg_partition_root to get top-most parent of a partition tree (https://commitfest.postgresql.org/22/1906/, Michael Paquier, details: https://www.depesz.com/2018/10/30/waiting-for-postgresql-12-add-pg_partition_tree-to-display-information-about-partitions/)
  - [x] Generalized expression syntax for partition bounds (https://commitfest.postgresql.org/20/1620/, Peter Eisentraut);
  - [x] Allow multi-inserts during COPY into a partitioned table (https://git.postgresql.org/gitweb/?p=postgresql.git;a=commitdiff;h=0d5f05cde011512e605bb2688d9b1fbb5b3ae152, Peter Eisentraut);
  - [x] Support foreign keys that reference partitioned tables (https://git.postgresql.org/gitweb/?p=postgresql.git;a=commitdiff;h=f56f8f8da6afd8523b4d5284e02a20ed2b33ef8d, Alvaro Herrera, details: https://www.depesz.com/2019/04/24/waiting-for-postgresql-12-support-foreign-keys-that-reference-partitioned-tables/)
  - [x] Expand run-time partition pruning to work with MergeAppend (https://commitfest.postgresql.org/18/1675/, https://git.postgresql.org/gitweb/?p=postgresql.git;a=commit;h=5220bb7533f9891b1e071da6461d5c387e8f7b09, David Rowley);

- ##### Pluggable Table Storage Interface
  - [x] CREATE ACCESS METHOD (https://commitfest.postgresql.org/22/1283/, Álvaro Herrera (alvherre), Andres Freund (andresfreund), Haribabu Kommi (haribabu), Alexander Korotkov (smagen))


- ##### Inlined WITH queries (Common table expressions):
  - [x] MATERIALIZED clause (https://www.depesz.com/2019/02/19/waiting-for-postgresql-12-allow-user-control-of-cte-materialization-and-change-the-default-behavior/).


- ##### JSON path queries per SQL/JSON specification
  - [x] JSON path queries (https://www.depesz.com/2019/03/19/waiting-for-postgresql-12-partial-implementation-of-sql-json-path-language/)

- ##### Indexing Performance, Functionality, and Management:
  - [x] ~~K-nearest neighbor~~; (Moved to next CF)
  - [x] ~~Changes in B-tree indexes~~; (Moved to next CF)
  - [x] Rebuilding indexes CONCURRENTLY (https://commitfest.postgresql.org/22/1921/);
  - [x] Generated Columns (https://www.depesz.com/2019/04/17/waiting-for-postgresql-12-generated-columns/, https://commitfest.postgresql.org/22/1443/)
  - [x] Collations (https://www.postgresql.org/message-id/flat/1ccc668f-4cbc-0bef-af67-450b47cdfee7@2ndquadrant.com)


- ##### Server-side features:
  - [ ] Page Checksums
  - [ ] WAL files for GiST, GIN, or SP-GiST indexes.
  - [ ] Most-common Value Extended Statistics
  - [ ] Authentication & Connection Security


## Used materials:
- https://pgconf.ru/media/2019/02/20/postgresql-12-pgconfru-2019-v2.pdf  
- https://www.postgresql.org/list/
- https://www.depesz.com

## Join PostgreSQL Ukraine on Telegram:
![QR](/images/qr-pglviv.png)
