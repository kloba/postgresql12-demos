## A way to find changes and examples in the PostgreSQL project

### 1. Check a list of patches on a Commit fest: https://commitfest.postgresql.org/22/?text=&status=-1&targetversion=2&author=-1&reviewer=-1&sortkey=
![CommitFest](/images/commitfest.png)

### 2. Try to find the same commit to PostgreSQL git repository: https://git.postgresql.org/gitweb/?p=postgresql.git;a=summary
