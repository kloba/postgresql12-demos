EXPLAIN analyze
WITH x AS MATERIALIZED (
    SELECT relkind, COUNT(*) FROM pg_class GROUP BY relkind
)
SELECT * FROM x WHERE relkind = 'r';
