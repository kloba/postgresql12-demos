-- Author: Hubert depesz Lubaczewski
-- URL: https://www.depesz.com/2018/10/30/waiting-for-postgresql-12-add-pg_partition_tree-to-display-information-about-partitions/

-- Let's imagine you have a table that has been partitioned:
CREATE TABLE users (
    id             serial NOT NULL,
    username       text NOT NULL,
    UNIQUE (username)
)
PARTITION BY RANGE ( username );

CREATE TABLE users_1 partition OF users (PRIMARY KEY (id))
    FOR VALUES FROM (minvalue) TO ('e');

CREATE TABLE users_2 partition OF users (PRIMARY KEY (id))
    FOR VALUES FROM ('e') TO ('s');

CREATE TABLE users_3 partition OF users (PRIMARY KEY (id))
    FOR VALUES FROM ('s') TO (maxvalue);

-- It's pretty simple, and all, yet \d doesn't show it clearly:
\d users 

-- Sure, I can \d+:
\d+ users

-- Let's assume we want to further partition users_1:
CREATE TABLE new_users_1 (LIKE users including ALL)
 partition BY range (username);


CREATE TABLE new_users_1_1 partition OF new_users_1 (PRIMARY KEY (id))
    FOR VALUES FROM (minvalue) TO ('a');

CREATE TABLE new_users_1_2 partition OF new_users_1 (PRIMARY KEY (id))
    FOR VALUES FROM ('a') TO ('c');

CREATE TABLE new_users_1_3 partition OF new_users_1 (PRIMARY KEY (id))
    FOR VALUES FROM ('c') TO (maxvalue);

DROP TABLE users_1;

ALTER TABLE new_users_1 RENAME TO users_1;

ALTER TABLE users attach partition users_1
    FOR VALUES FROM (MINVALUE) TO ('e');

-- Now, our old users_1 has been subpartitioned, and \d+ of users no longer shows all partitions:
\d+ users

SELECT * FROM pg_partition_tree('users');
