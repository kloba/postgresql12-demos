-- Author: Hubert depesz Lubaczewski
-- URL: https://www.depesz.com/2019/04/24/waiting-for-postgresql-12-support-foreign-keys-that-reference-partitioned-tables/

CREATE TABLE users (
    id             serial PRIMARY KEY,
    username       text NOT NULL,
    password       text
)
PARTITION BY RANGE ( id );

CREATE TABLE users_0
    partition OF users
    FOR VALUES FROM (MINVALUE) TO (10);

CREATE TABLE users_1
    partition OF users
    FOR VALUES FROM (10) TO (20);

INSERT INTO users (username) SELECT 'user #' || i FROM generate_series(1,15) i;

-- Sanity check that data got distributed:
SELECT tableoid::regclass, id, username
FROM users
ORDER BY id;

-- OK. Looks that we have data properly spread across both partitions.
-- And now, let's try to make the table that has fkey to users:

CREATE TABLE test (
    id serial PRIMARY KEY,
    user_id int4 NOT NULL REFERENCES users (id)
);

-- SUCCESS.
-- So, let's test it. First, make sure we know correct user ids:

SELECT string_agg(id::text, ', ' ORDER BY id)
FROM users;

-- OK. So, these two should work:
INSERT INTO test (user_id) VALUES (2);
INSERT INTO test (user_id) VALUES (14);

-- but this should fail:
INSERT INTO test (user_id) VALUES (100);

-- And it does!
-- Just to be safe, let's also test if it will correctly stop me from deleting used user:

DELETE FROM users WHERE id = 2;
