CREATE TABLE products (
    product_id     serial NOT NULL,
    product_code   character varying(32) NOT NULL,
    category_id    integer NOT NULL,
    created		   timestamp NOT NULL,
	PRIMARY KEY (product_id, category_id)
)
PARTITION BY LIST (category_id);
