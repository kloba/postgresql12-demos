START TRANSACTION isolation level read committed;

SELECT *
FROM listp;

-- sleep for 30 seconds
SELECT pg_sleep(30);

-- new partition visible while in transaction
SELECT *
FROM listp;
