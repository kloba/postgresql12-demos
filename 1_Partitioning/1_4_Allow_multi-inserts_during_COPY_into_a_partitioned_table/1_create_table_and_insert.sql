-- Author: Peter Eisentraut
-- URL: https://git.postgresql.org/gitweb/?p=postgresql.git;a=commitdiff;h=0d5f05cde011512e605bb2688d9b1fbb5b3ae152s

-- test copy from with a partitioned table
create table parted_copytest (
   a int,
   b int,
   c text
) partition by list (b);

create table parted_copytest_a1 (c text, b int, a int);
create table parted_copytest_a2 (a int, c text, b int);

alter table parted_copytest attach partition parted_copytest_a1 for values in(1);
alter table parted_copytest attach partition parted_copytest_a2 for values in(2);

-- We must insert enough rows to trigger multi-inserts.  These are only
-- enabled adaptively when there are few enough partition changes.
insert into parted_copytest select x,1,'One' from generate_series(1,10000000) x;
insert into parted_copytest select x,2,'Two' from generate_series(10000001,20000000) x;
insert into parted_copytest select x,1,'One' from generate_series(20000001,30000000) x;

\timing on

copy (select * from parted_copytest order by a) to '/tmp/parted_copytest.csv';

\timing off

truncate parted_copytest;

\timing on
copy parted_copytest from '/tmp/parted_copytest.csv';
\timing off

select tableoid::regclass,count(*),sum(a)
from parted_copytest
group by tableoid
order by tableoid::regclass::name;
