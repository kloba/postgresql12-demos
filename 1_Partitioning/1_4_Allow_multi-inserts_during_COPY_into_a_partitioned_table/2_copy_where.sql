truncate parted_copytest;

-- COPY WITH WHERE
copy parted_copytest from '/tmp/parted_copytest.csv' where b = 2;

select tableoid::regclass,count(*),sum(a)
from parted_copytest
group by tableoid
order by tableoid::regclass::name;
