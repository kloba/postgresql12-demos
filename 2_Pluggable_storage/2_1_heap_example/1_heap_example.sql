CREATE EXTENSION pageinspect;

DROP TABLE IF EXISTS users ;
CREATE TABLE users (ID int, First_name character varying(50), Last_name character varying(50));

INSERT INTO users VALUES (1, 'Taras', 'Kloba');
INSERT INTO users VALUES (2, 'Roman', 'Lysyy');

UPDATE users
SET Last_name = 'Klioba'
WHERE Last_name = 'Kloba';

UPDATE users
SET Last_name = 'Kloba'
WHERE Last_name = 'Klioba';

DELETE FROM users
WHERE First_name = 'Roman';

SELECT ctid, *
FROM users;

-- big data :)
SELECT * FROM heap_page_items(get_raw_page('users', 0));

SELECT encode(t_data, 'escape') FROM heap_page_items(get_raw_page('users', 0));

VACUUM users;

SELECT * FROM heap_page_items(get_raw_page('users', 0));

VACUUM FULL users;

SELECT * FROM heap_page_items(get_raw_page('users', 0));
