CREATE TABLE concur_reindex_part1 (c1 int) PARTITION BY RANGE (c1);
CREATE TABLE concur_reindex_part1v1 PARTITION OF concur_reindex_part1 FOR VALUES FROM (0) TO (100);
CREATE INDEX concur_reindex_idx1_part1 ON concur_reindex_part1 (c1);
REINDEX TABLE CONCURRENTLY concur_reindex_part1v1;
DROP TABLE concur_reindex_part1;
