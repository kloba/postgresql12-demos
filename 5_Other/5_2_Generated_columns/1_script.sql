DROP TABLE IF EXISTS tvs;

CREATE TABLE tvs (
    id serial PRIMARY KEY,
	model character varying(50),
    diagonal_inches int4 NOT NULL DEFAULT 0,
    diagonal_cm int4 generated always AS (diagonal_inches * 2.4) stored
);

INSERT INTO tvs (model, diagonal_inches) VALUES ('Samsung 7402', 43);
INSERT INTO tvs (model, diagonal_inches) VALUES ('Samsung 7400', 55);

SELECT *
FROM tvs;
