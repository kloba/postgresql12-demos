CREATE COLLATION case_sensitive (provider = icu, locale = 'und');
CREATE COLLATION case_in_sensitive (provider = icu, locale = 'und-u-ks-level2', deterministic = false);

SELECT 'abc' <= 'ABC' COLLATE case_sensitive, 'abc' >= 'ABC' COLLATE case_sensitive;
SELECT 'abc' <= 'ABC' COLLATE case_in_sensitive, 'abc' >= 'ABC' COLLATE case_in_sensitive;

CREATE TABLE test1cs (x text COLLATE case_sensitive);
CREATE TABLE test2cs (x text COLLATE case_sensitive);
CREATE TABLE test3cs (x text COLLATE case_sensitive);

INSERT INTO test1cs VALUES ('abc'), ('def'), ('ghi');
INSERT INTO test2cs VALUES ('ABC'), ('ghi');
INSERT INTO test3cs VALUES ('abc'), ('ABC'), ('def'), ('ghi');

SELECT x FROM test1cs UNION SELECT x FROM test2cs;
SELECT x FROM test2cs UNION SELECT x FROM test1cs;
SELECT x FROM test1cs INTERSECT SELECT x FROM test2cs;
SELECT x FROM test2cs INTERSECT SELECT x FROM test1cs;
SELECT x FROM test1cs EXCEPT SELECT x FROM test2cs;
SELECT x FROM test2cs EXCEPT SELECT x FROM test1cs;
SELECT x, count(*) FROM test3cs GROUP BY x ORDER BY x;

CREATE TABLE test1ci (x text COLLATE case_in_sensitive);
CREATE TABLE test2ci (x text COLLATE case_in_sensitive);
CREATE TABLE test3ci (x text COLLATE case_in_sensitive);

INSERT INTO test1ci VALUES ('abc'), ('def'), ('ghi');
INSERT INTO test2ci VALUES ('ABC'), ('ghi');
INSERT INTO test3ci VALUES ('abc'), ('ABC'), ('def'), ('ghi');

SELECT x FROM test1ci UNION SELECT x FROM test2ci;
SELECT x FROM test2ci UNION SELECT x FROM test1ci;
SELECT x FROM test1ci INTERSECT SELECT x FROM test2ci;
SELECT x FROM test2ci INTERSECT SELECT x FROM test1ci;
SELECT x FROM test1ci EXCEPT SELECT x FROM test2ci;
SELECT x FROM test2ci EXCEPT SELECT x FROM test1ci;
SELECT x, count(*) FROM test3ci GROUP BY x ORDER BY x;
SELECT x, row_number() OVER (ORDER BY x), rank() OVER (ORDER BY x) FROM test3ci ORDER BY x;
